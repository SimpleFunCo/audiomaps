package com.gmail.simplefunco.audiomaps.Helper;
/* Created on 4/4/2017. */

import com.gmail.simplefunco.audiomaps.Controller.Callback.GeoApi;
import com.gmail.simplefunco.audiomaps.Model.CityPojo;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.gmail.simplefunco.audiomaps.Helper.Constants.HTTP.GEO_URL;

public class RetrofitManager {

    public GeoApi initRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GEO_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(GeoApi.class);
    }
}
