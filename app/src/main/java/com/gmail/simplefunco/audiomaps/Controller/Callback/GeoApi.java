package com.gmail.simplefunco.audiomaps.Controller.Callback;
/* Created on 4/4/2017. */

import com.gmail.simplefunco.audiomaps.Model.TotalResultPojo;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface GeoApi {

    @GET("searchJSON")
    Call<TotalResultPojo> getCities(@QueryMap Map<String, String> options);
}
