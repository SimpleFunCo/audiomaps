package com.gmail.simplefunco.audiomaps.Helper;
/* Created on 4/4/2017. */

public class Constants {
    public static class HTTP {
        public static final String GEO_URL = "http://api.geonames.org/";
        public static final String USERNAME = "simplefun";
        public static final String COUNTRY = "ua";
        public static final String MAX_ROWS = "1000";
        public static final String JSON_STYLE = "SHORT";
    }

    public static class CODES {
        public static final int REQ_PERMISSION = 200;
    }
}
