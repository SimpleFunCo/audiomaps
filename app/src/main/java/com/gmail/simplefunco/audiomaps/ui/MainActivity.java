package com.gmail.simplefunco.audiomaps.ui;

import android.Manifest;
import android.app.NotificationManager;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Toast;

import com.gmail.simplefunco.audiomaps.Controller.Callback.GeoApi;
import com.gmail.simplefunco.audiomaps.Controller.ClusterIconRenderer;
import com.gmail.simplefunco.audiomaps.Helper.Constants;
import com.gmail.simplefunco.audiomaps.Helper.RetrofitManager;
import com.gmail.simplefunco.audiomaps.Model.CityPojo;
import com.gmail.simplefunco.audiomaps.Model.TotalResultPojo;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gmail.simplefunco.audiomaps.Helper.Constants.CODES.REQ_PERMISSION;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback,
        SensorEventListener {

    private static final String TAG = "MainActivityLogs";

    @BindView(R.id.ibtnRewind)
    ImageButton ibtnRewind;
    @BindView(R.id.ibtnPlayPause)
    ImageButton ibtnPlayPause;
    @BindView(R.id.sbAudioSeek)
    SeekBar sbAudioSeek;
    @BindView(R.id.pbLoadingMarkers)
    ProgressBar pbLoadingMarkers;
    @BindView(R.id.chbAutoRotate)
    CheckBox chbAutoRotate;

    private MediaPlayer mMediaPlayer;
    private Handler handler;
    private List<CityPojo> mCityPojos;
    private TotalResultPojo mResultPojo;
    private GoogleMap googleMap;
    private Runnable waitForJson;
    private final Object lock = new Object();
    private ClusterManager<CityPojo> mClusterManager;
    private SensorManager mSensorManager;
    private Sensor mRotationVector;
    private float azimut = 0;
    private final Object rotationLock = new Object();
    private CameraPosition cameraPosition;
    private boolean rotating = false;
    private int notifyCityId = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_map);

        ButterKnife.bind(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initMediaPlayer();
        initMediaControls();

        RetrofitManager retrofitManager = new RetrofitManager();
        GeoApi geoApi = retrofitManager.initRetrofit();

        Map<String, String> jsonQuery = new HashMap<>();
        jsonQuery.put("username", Constants.HTTP.USERNAME);
        jsonQuery.put("country", Constants.HTTP.COUNTRY);
        jsonQuery.put("maxRows", Constants.HTTP.MAX_ROWS);
        jsonQuery.put("style", Constants.HTTP.JSON_STYLE);

        Call<TotalResultPojo> cities = geoApi.getCities(jsonQuery);

        cities.enqueue(new Callback<TotalResultPojo>() {
            @Override
            public void onResponse(Call<TotalResultPojo> call, Response<TotalResultPojo> response) {
                synchronized (lock) {
                    mCityPojos = new ArrayList<>();
                    mResultPojo = response.body();
                    mCityPojos.addAll(mResultPojo.getGeonames());
                    handler.post(waitForJson);

                    lock.notify();
                }
            }

            @Override
            public void onFailure(Call<TotalResultPojo> call, Throwable t) {

            }
        });

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mRotationVector = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSensorManager.registerListener(this, mRotationVector, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void initMediaControls() {
        ibtnPlayPause.setOnClickListener(item -> {
            if (!mMediaPlayer.isPlaying()) {
                ibtnPlayPause.setImageDrawable(ContextCompat.getDrawable(this, android.R.drawable.ic_media_pause));
                mMediaPlayer.start();
            } else {
                ibtnPlayPause.setImageDrawable(ContextCompat.getDrawable(this, android.R.drawable.ic_media_play));
                mMediaPlayer.pause();
            }
        });

        ibtnRewind.setOnClickListener(item -> {
            mMediaPlayer.seekTo(0);
            sbAudioSeek.setProgress(0);
        });

        sbAudioSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mMediaPlayer.seekTo(seekBar.getProgress());
                seekBar.setProgress(seekBar.getProgress());
            }
        });
    }

    private void initMediaPlayer() {
        mMediaPlayer = MediaPlayer.create(this, R.raw.iogann_s_na_golubom_dunae);
        handler = new Handler();
        sbAudioSeek.setMax(mMediaPlayer.getDuration());

        Runnable seekBarThread = new Runnable() {
            public void run() {
                if (mMediaPlayer.isPlaying()) {
                    sbAudioSeek.setMax(mMediaPlayer.getDuration());
                    sbAudioSeek.setProgress(mMediaPlayer.getCurrentPosition());

                    handler.postDelayed(this, 100);
                }
            }
        };

        handler.removeCallbacks(seekBarThread);
        handler.postDelayed(seekBarThread, 100);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLngBounds ukraine = new LatLngBounds(
                new LatLng(44.390415, 22.128889),
                new LatLng(52.369362, 40.207390));

        this.googleMap = googleMap;

        googleMap.setOnInfoWindowClickListener(marker -> {
            NotificationCompat.Builder cityNotify =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(android.R.drawable.sym_def_app_icon)
                            .setContentTitle("Selected city")
                            .setContentText(marker.getTitle());

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            notificationManager.notify(notifyCityId, cityNotify.build());
        });

        if (checkPermission()) {
            googleMap.setMyLocationEnabled(true);
        } else {
            askPermission();
        }
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        waitForJson = () -> {

            synchronized (lock) {
                while (mCityPojos == null) {
                    try {
                        lock.wait(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            setUpClusterer();
        };
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ukraine.getCenter(), 5));
    }

    public void addMarker(LatLng position, String title) {
        googleMap.addMarker(new MarkerOptions()
                .position(position)
                .anchor(0.5f, 0.5f)
                .title(title));
    }

    private void setUpClusterer() {
        googleMap.clear();
        mClusterManager = new ClusterManager<>(this, googleMap);

        googleMap.setOnCameraIdleListener(mClusterManager);
        googleMap.setOnMarkerClickListener(mClusterManager);

        mClusterManager.setOnClusterItemClickListener(cityPojo -> {
            NotificationCompat.Builder cityNotify =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(android.R.drawable.sym_def_app_icon)
                            .setContentTitle("Selected city")
                            .setContentText(cityPojo.getName());

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            notificationManager.notify(notifyCityId, cityNotify.build());
            return false;
        });

        for (CityPojo city : mCityPojos) {
            mClusterManager.addItem(city);
        }
        mClusterManager.setRenderer(
                new ClusterIconRenderer(getApplicationContext(), googleMap, mClusterManager));
        mClusterManager.setAnimation(false);
        pbLoadingMarkers.setVisibility(View.INVISIBLE);
    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED);
    }

    private void askPermission() {
        ActivityCompat.requestPermissions(
                this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQ_PERMISSION
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQ_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (checkPermission())
                        googleMap.setMyLocationEnabled(true);

                } else {
                    Toast.makeText(getApplicationContext(), "Permission not granted", Toast.LENGTH_SHORT).show();

                }
                break;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (checkPermission()) {
            googleMap.setMyLocationEnabled(false);
        }
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float[] mGravity = {};
        float[] mGeomagnetic = {};


        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = sensorEvent.values;
        if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = sensorEvent.values;
        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            SensorManager.getRotationMatrixFromVector(R, sensorEvent.values);
            float orientation[] = new float[3];
            SensorManager.getOrientation(R, orientation);
            synchronized (rotationLock) {
                azimut = (int) Math.round(Math.toDegrees(orientation[0]));
            }
        }

        Runnable rotateMap = () -> {
            synchronized (rotationLock) {
                cameraPosition = new CameraPosition.Builder()
                        .target(googleMap.getCameraPosition().target)
                        .zoom(googleMap.getCameraPosition().zoom)
                        .bearing(azimut)
                        .build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                rotating = false;
            }
        };

        if (!rotating && chbAutoRotate.isChecked()) {
            handler.postDelayed(rotateMap, 500);
            rotating = true;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
