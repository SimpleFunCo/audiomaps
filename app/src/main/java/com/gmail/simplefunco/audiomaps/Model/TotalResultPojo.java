package com.gmail.simplefunco.audiomaps.Model;
/* Created on 4/4/2017. */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TotalResultPojo {

    @SerializedName("totalResultsCount")
    @Expose
    private Integer totalResultsCount;
    @SerializedName("geonames")
    @Expose
    private List<CityPojo> geonames = null;

    public Integer getTotalResultsCount() {
        return totalResultsCount;
    }

    public void setTotalResultsCount(Integer totalResultsCount) {
        this.totalResultsCount = totalResultsCount;
    }

    public List<CityPojo> getGeonames() {
        return geonames;
    }

    public void setGeonames(List<CityPojo> geonames) {
        this.geonames = geonames;
    }
}
