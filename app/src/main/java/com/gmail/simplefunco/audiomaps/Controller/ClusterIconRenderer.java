package com.gmail.simplefunco.audiomaps.Controller;
/* Created on 4/4/2017. */

import android.content.Context;

import com.gmail.simplefunco.audiomaps.Model.CityPojo;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

public class ClusterIconRenderer extends DefaultClusterRenderer<CityPojo> {
    private GoogleMap mGoogleMap;
    private float azimut;

    public ClusterIconRenderer(Context context, GoogleMap map,
                               ClusterManager<CityPojo> clusterManager) {
        super(context, map, clusterManager);
    }

    @Override
    protected void onBeforeClusterItemRendered(CityPojo item, MarkerOptions markerOptions) {
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(android.R.drawable.sym_def_app_icon);
        markerOptions.icon(icon);
        super.onBeforeClusterItemRendered(item, markerOptions);
    }
}
